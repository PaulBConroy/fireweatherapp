package com.paulconroy.fireweatherapp;


import android.support.annotation.NonNull;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.Locale;

/**
 * General App-Wide Util functions
 * Created by paulconroy on 11/03/2018.
 */

public class Utils {

    /**
     * Convert the coordinates from a double to a string
     * @param coor the double coordinate
     * @return the string coordinate
     */
    public static String coorFormat(final double coor) {
        return String.valueOf(coor);
    }

    /**
     * Format the degrees according to the user's device
     * ie Imperial or Metric
     * @param degree the degree value
     * @return the formatted degree value
     */
    public static String degFormat(@NonNull final String degree) {
        if (getLocaleUnits().equalsIgnoreCase(Constants.IMPERIAL)) {
            return String.valueOf((int) Double.parseDouble(degree)) + Constants.DEGREES_FAHRENHEIT;
        }
        return String.valueOf((int) Double.parseDouble(degree)) + Constants.DEGREES_CELSIUS;
    }

    /**
     * Returns the time of the day using the provided epoch
     * @param millis the millisecs
     * @return the time of the day as a string
     */
    public static String getTimeOfDay(final long millis) {
        DateTime dateTimeAsString = new DateTime(millis * 1000, DateTimeZone.UTC );
        int hour = dateTimeAsString.getHourOfDay();
        int minuteOfHour = dateTimeAsString.getMinuteOfHour();
        String time = String.valueOf(hour) + ":" +String.valueOf(minuteOfHour);
        return String.valueOf(time);
    }

    /**
     * Returns the user's correct units to display throughout the app
     * @return the string unit
     */
    public static String getLocaleUnits() {
        String countryCode = Locale.getDefault().getCountry();
        if (countryCode.equalsIgnoreCase(Constants.US_CODE) ||
                countryCode.equalsIgnoreCase(Constants.LR_CODE) ||
                countryCode.equalsIgnoreCase(Constants.MM_CODE)) {
            return Constants.IMPERIAL;
        } else {
            return Constants.METRIC;
        }
    }

    /**
     * Using the device's local to provide the correct unit, return the correct speed format
     * @param speed the speed value as a double
     * @return the formatted speed value as a string ready to be displayed
     */
    public static String getWindSpeedFormat(final double speed) {
        String countryCode = Locale.getDefault().getCountry();
        if (countryCode.equalsIgnoreCase(Constants.US_CODE) ||
                countryCode.equalsIgnoreCase(Constants.LR_CODE) ||
                countryCode.equalsIgnoreCase(Constants.MM_CODE)) {
            return String.valueOf((int) speed) + Constants.MPH;
        } else {
            return String.valueOf((int) speed) + Constants.KMH;
        }
    }

}
