package com.paulconroy.fireweatherapp;

/**
 * Constant values used through-out the app
 * Created by paulconroy on 11/03/2018.
 */

public final class Constants {

    /**
     * Base url for API used to retrieve weather data
     */
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    /**
     * Constant format of degrees CELSIUS used for temperature string output
     */
    public static final String DEGREES_CELSIUS = (char) 0x00B0 + "C";

    /**
     * Constant format of degrees FAHRENHEIT used for temperature string output
     */
    public static final String DEGREES_FAHRENHEIT = (char) 0x00B0 + "F";

    /**
     * Constant String format of latitude used to assign latitude value to retrofit API endpoint
     */
    public static final String LAT = "lat";

    /**
     * Constant String format of longitude used to assign latitude value to retrofit API endpoint
     */
    public static final String LON = "lon";

    /**
     * Constant String format of units used to assign correct unit value to retrofit API endpoint
     */
    public static final String UNITS = "units";

    /**
     * Constant String format of metric used to assign correct unit value to retrofit API endpoint
     */
    public static final String METRIC = "metric";

    /**
     * Constant String format of imperial used to assign correct unit value to retrofit API endpoint
     */
    public static final String IMPERIAL = "imperial";

    /**
     * Constant String format of app api key used to assign authorisation to retrofit API endpoint
     */
    public static final String APP_ID = "appid";

    /**
     * Constant String format of US Code to detect locale unit
     */
    public static final String US_CODE = "US";

    /**
     * Constant String format of LR Code to detect locale unit
     */
    public static final String LR_CODE = "LR";

    /**
     * Constant String format of MM Code to detect locale unit
     */
    public static final String MM_CODE = "MM";

    /**
     * Constant String format of KMH
     */
    public static final String KMH = "kmh";

    /**
     * Constant String format of MPH
     */
    public static final String MPH = "mph";

    /**
     * String format of the base url to get the weather icon
     */
    public static final String ICON_BASE_URL = "http://openweathermap.org/img/w/";

    /**
     * String format of the file type associated with the weather icon
     */
    public static final String PNG = ".png";

    /**
     * Default const
     */
    private Constants() {
        //ignore
    }
}
