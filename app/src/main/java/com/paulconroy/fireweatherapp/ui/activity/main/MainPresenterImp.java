package com.paulconroy.fireweatherapp.ui.activity.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.paulconroy.fireweatherapp.BuildConfig;
import com.paulconroy.fireweatherapp.Constants;
import com.paulconroy.fireweatherapp.Utils;
import com.paulconroy.fireweatherapp.api.ApiClient;
import com.paulconroy.fireweatherapp.model.Result;

import java.util.LinkedHashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Implementation to handle business logic for the Main Activity
 * Created by paulconroy on 11/03/2018.
 */

class MainPresenterImp implements MainPresenter, Callback<Result>, LocationListener {

    private MainView mainView;
    private LocationManager locationManager;
    private Result result;
    private Context context;

    public MainPresenterImp(@NonNull final MainView mainView, @NonNull final Context context) {
        this.mainView = mainView;
        this.context = context;
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onLocationChanged(@NonNull final Location location) {
        getWeather(Utils.coorFormat(location.getLatitude()),Utils.coorFormat(location.getLongitude()));
    }

    @Override
    public void onStatusChanged(@NonNull final String s, final int i, @NonNull final Bundle bundle) {
        //ignore
    }

    @Override
    public void onProviderEnabled(@NonNull final String s) {
        //ignore
    }

    @Override
    public void onProviderDisabled(@NonNull final String s) {
        //ignore
    }

    @Override
    public void onResponse(@NonNull final Call<Result> call,
                           @NonNull final Response<Result> response) {
        result = response.body();
        //Let the view handle the result to UI
        getView().handleUI(result);
    }

    @Override
    public void onFailure(@NonNull final Call<Result> call, @NonNull final Throwable t) {
       Log.e("ERROR", t.getMessage());
    }

    /**
     * Return the view
     * @return the main view
     */
    private MainView getView() {
        return this.mainView;
    }

    /**
     * Construct the LinkedHashMap with the necessary parameters and use retrofit to retrieve
     * the weather information from the provided endpoint
     *
     * @param lat the latitude on the device
     * @param lon the longitude on the device
     */
    private void getWeather(@NonNull final String lat, @NonNull final String lon) {
        ApiClient apiClient = new ApiClient();

        Map<String, String> map = new LinkedHashMap<>();
        map.put(Constants.LAT, lat);
        map.put(Constants.LON, lon);
        map.put(Constants.UNITS, Utils.getLocaleUnits());
        map.put(Constants.APP_ID, BuildConfig.MY_WEATHER_KEY);
        apiClient.getController().getWeather(map).enqueue(this);
    }

    /**
     * Permission is handled on the UI
     */
    @SuppressLint("MissingPermission")
    @Override
    public void getLocation() {
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        //If device has last known location
        if (location != null) {
            getWeather(Utils.coorFormat(location.getLatitude()), Utils.coorFormat(location.getLongitude()));
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }
}
