package com.paulconroy.fireweatherapp.ui.activity.main;

/**
 * Presenter Interface for Main Activity
 * Created by paulconroy on 11/03/2018.
 */

interface MainPresenter {

    /**
     * Retrieve the weather information
     */
    void getLocation();
}
