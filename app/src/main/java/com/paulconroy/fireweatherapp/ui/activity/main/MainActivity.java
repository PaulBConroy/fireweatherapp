package com.paulconroy.fireweatherapp.ui.activity.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.paulconroy.fireweatherapp.Constants;
import com.paulconroy.fireweatherapp.R;
import com.paulconroy.fireweatherapp.Utils;
import com.paulconroy.fireweatherapp.base.BaseActivity;
import com.paulconroy.fireweatherapp.model.Result;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * MainActivity -> Displays the weather information based on the user's location
 *
 * UI Logic is handled here.
 **/
public class MainActivity extends BaseActivity<MainPresenter> implements MainView {

    @BindView(R.id.ivWeatherIcon)
    protected ImageView ivWeatherIcon;
    @BindView(R.id.tvDescription)
    protected TextView tvDescription;
    @BindView(R.id.tvTemp)
    protected TextView tvTemp;
    @BindView(R.id.tvName)
    protected TextView tvName;
    @BindView(R.id.tvTempMinMax)
    protected TextView tvTempMinMax;
    @BindView(R.id.tvSunInfo)
    protected TextView tvSunInfo;
    @BindView(R.id.tvWindInfo)
    protected TextView tvWindInfo;

    public static final int REQUEST_LOCATION = 99;
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        checkLocationPermission();
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        getPresenter().getLocation();
                    }

                } else {
                    Toast.makeText(this, getResources().getString(R.string.location_permission),
                            Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Check if the location permission has been approved by the user, if so, retrieve the
     * location
     */
    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
            return;
        }

        getPresenter().getLocation();
    }

    @Override
    protected MainPresenter getPresenter() {
        if (presenter == null) {
            presenter = new MainPresenterImp(this, this);
        }

        return presenter;
    }

    @Override
    public void handleUI(@NonNull final Result result) {
        String url = Constants.ICON_BASE_URL + result.getWeather().get(0).getIcon() + Constants.PNG;

        //Handle the information and apply it to the appropriate UI
        Picasso.get()
                .load(url)
                .into(ivWeatherIcon);
        tvDescription.setText(result.getWeather().get(0).getDescription());
        tvTemp.setText(String.format(getResources().getString(R.string.temp_placeholder),
                Utils.degFormat(result.getMain().getTemp().toString())));
        tvName.setText(result.getName());
        tvTempMinMax.setText(String.format(getResources().getString(R.string.temp_min_max),
                Utils.degFormat(result.getMain().getTempMin().toString()),
                Utils.degFormat(result.getMain().getTempMax().toString())));
        tvSunInfo.setText(String.format(getResources().getString(R.string.sun_info),
                Utils.getTimeOfDay(result.getSys().getSunrise()),
                Utils.getTimeOfDay(result.getSys().getSunset())));
        tvWindInfo.setText(String.format(getResources().getString(R.string.wind_info),
                Utils.getWindSpeedFormat(result.getWind().getSpeed())));
    }
}
