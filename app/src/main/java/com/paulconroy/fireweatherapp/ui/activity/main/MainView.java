package com.paulconroy.fireweatherapp.ui.activity.main;

import android.support.annotation.NonNull;

import com.paulconroy.fireweatherapp.model.Result;

/**
 * View Interface to invoke UI logic from the presenter
 * Created by paulconroy on 11/03/2018.
 */

interface MainView {

    /**
     * Handle the passed information and apply it to the appropriate UI
     * @param result the retrieved weather information
     */
    void handleUI(@NonNull final Result result);

}
