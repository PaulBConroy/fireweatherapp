package com.paulconroy.fireweatherapp.base;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import java.lang.ref.WeakReference;

/**
 * All activities will extend from this
 * Created by paulconroy on 11/03/2018.
 **/

public abstract class BaseActivity<P> extends AppCompatActivity {

    protected P presenter;
    private WeakReference<? extends Context> weakRefContext;

    /**
     * Get the current Presenter Instance
     *
     * @return The current Presenter Instance
     */
    protected abstract P getPresenter();

    /**
     * Set the current Presenter instance
     *
     * @param presenter The given Presenter instance
     */
    protected void setPresenter(P presenter) {
        this.presenter = presenter;
    }

    public WeakReference<? extends Context> getViewContext() {
        if (weakRefContext == null) {
            weakRefContext = new WeakReference<>(BaseActivity.this);
        }
        return weakRefContext;
    }
}
