package com.paulconroy.fireweatherapp.api;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paulconroy.fireweatherapp.Constants;
import com.paulconroy.fireweatherapp.model.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * API Client used to set the controller and invoke controller methods
 * Created by paulconroy on 11/03/2018.
 */

public class ApiClient implements Callback<Result> {

    private Result result;
    private ApiController apiController;

    @Override
    public void onResponse(@NonNull final Call<Result> call, @NonNull final Response<Result> response) {
        Log.d("Result callback", "Success");
        result = response.body();
    }

    @Override
    public void onFailure(@NonNull final Call<Result> call, @NonNull final Throwable t) {
        Log.e("Result callback", "Failure");
    }

    /**
     * Constructs the api controller
     */
    public void setController() {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        apiController = retrofit.create(ApiController.class);
    }

    /**
     * returns an instance of the api controller
     * @return the api controller
     */
    public ApiController getController() {
        if (apiController == null) {
            setController();
        }
        return this.apiController;
    }
}
