package com.paulconroy.fireweatherapp.api;

import com.paulconroy.fireweatherapp.model.Result;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * The controller is used to host the methods used for Retrofit to retrieve weather information
 * Created by paulconroy on 11/03/2018.
 */

public interface ApiController {

    /**
     * Fetch the results for the weather using the lat and long values
     */
    @GET("weather")
    Call<Result> getWeather(@QueryMap Map<String, String> map);
}
